var express = require('express');
var router = express.Router();
var bicicletaController = require('../../controllers/api/bicicletaControllerAPI');

router.get('/', bicicletaController.Bicicleta_list);
router.post('/create', bicicletaController.Bicicleta_create);
router.post('/delete', bicicletaController.Bicicleta_delete);
/*
router.get('/:id/update', bicicletaController.Bicicleta_update_get);
router.post('/:id/update', bicicletaController.Bicicleta_update_post);
router.post('/:id/delete', bicicletaController.Bicicleta_delete_post);*/

module.exports = router;